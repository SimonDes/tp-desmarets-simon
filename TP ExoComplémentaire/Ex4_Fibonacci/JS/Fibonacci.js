/*
    Desmarets Simon
    Suite de Fibonacci
    Calcule et affiche la suite de fibanacci en fonction de la limite voulu
*/

//Déclaration variable fin de la suite et saisie clavier
let FinSuite = prompt("Jusque combien ? ");
//Stockage de la fonction calculFibo dans la variabke
let Suite = calculFibo(FinSuite);
//Affichage de la suite
alert(Suite);


//Fonction retournant la suite de Fibonacci
function calculFibo(fin) { // Avec un prm celui de la fin de suite
    //Vecteur de la suite
    let vectSuite = [];
    //Dans la suite la première et la deuxième case on toujours les mêmes valeurs
    vectSuite [0] = 0;
    vectSuite [1] = 1;
    //Valeur intermédiaire pour le calcul
    let valinter1 = 0;
    let valinter2 = 0;
    //Variable de retour
    let res = "";

    for(let i = 2; i < fin; i++){       //Stockage dans le vecteur les valeur de la suite
        valinter1 = vectSuite[i-1];
        valinter2 = vectSuite[i-2];
        vectSuite[i] = valinter1 + valinter2;
    }

    for(let j=0 ;j < fin; j++){         //Afficher ceux qu'il y a dans le vecteur
        res += vectSuite[j] + " ";  
    }
    
    return res;
}