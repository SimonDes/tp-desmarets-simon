/*
    Desmarets Simon
    Affichage alphabet majuscule
*/
//Stockage de la fonction retournant l'alphabet
let alphabet = AffAlphabet();
//Affichage de la vairable
alert(alphabet);


function AffAlphabet() {    //Fonction retournant l'alphabet en majuscule
    const nbLettreAlp = 26; //Constante nombre de lettre dans l'alphabet
    let code = 65;          //Codage du caractère "A" en unicode
    let lettre = String.fromCharCode(code); //caractère "A"
    let res = "";           //Variable de retour

    for(let i = 1; i <= nbLettreAlp; i++){
        res += lettre + " ";
        code = 65+i;
        lettre = String.fromCharCode(code);
    }

    return res;
}