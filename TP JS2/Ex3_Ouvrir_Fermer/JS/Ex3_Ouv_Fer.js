/*
    Desmarets Simon
    Ouvre ou ferme la div quand on appuie sur le btn
*/
$(document).ready(function () {

    let etat = false;           //Variable état de la div 
    $("#maDiv1").hide("slow");  //cacher la div
    $("#btn1").click(  
        function () {      
            if(etat == false){  //Si la div est caché
                $("#maDiv1").show("slow");  //Afficher la div
                $("#btn1").attr("value", "Fermer"); //Changer le nom du bouton
                etat = true;        //Passer l'état à true
            }else{
                $("#maDiv1").hide("slow");  //Cacher la div
                $("#btn1").attr("value", "Ouvrir"); //Changer le nom du bouton
                etat = false;   //Passer l'état à false
            }
        
        })    

});
