/*
    Desmarets Simon
    Ajoute ou suprrimer des ligne d'un tableau 
    grâce au click sur le bouton
*/
$(document).ready(function () {

    $("#btnAdd").click(Ajouter);        //Click sur btnAdd appel la fonction Ajouter
    $("#btnDel").click(Supprimer);      //Click sur btnDel appel la fonction Supprimer
        
        
        function Ajouter() { 
            //Ajoute à la fin de tab la ligne souhaité     
            $("#tab").append("<tr><td><center> 1 </center></td></tr>");
        }

        function Supprimer() {
            //Supprimer la dernière ligne   
            $("tr:last").remove();
        }  

});
