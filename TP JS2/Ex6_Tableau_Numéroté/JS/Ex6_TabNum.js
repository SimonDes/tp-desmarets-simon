/*
    Desmarets Simon
    Ajoute ou suprrimer des ligne d'un tableau en affichant
    son numéros de ligne grâce au click sur le bouton 
*/
$(document).ready(function () {

    $("#btnAdd").click(Ajouter);        //Click sur btnAdd appel la fonction Ajouter
    $("#btnDel").click(Supprimer);      //Click sur btnDel appel la fonction Supprimer
        
        
        function Ajouter() { 
            let res = $("tr").length;   //Compte le nombre de ligne 
            //Ajoute à la fin de tab la ligne souhaité      
            $("#tab").append("<tr><td><center> " + res + " </center></td></tr>");
        }

        function Supprimer() {
            //Supprimer la dernière ligne   
            $("tr:last").remove();
        }  

});
