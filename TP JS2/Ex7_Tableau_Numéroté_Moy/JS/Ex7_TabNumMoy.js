/*
    Desmarets Simon
    Ajoute ou suprrimer des ligne d'un tableau en affichant
    son numéros de ligne et affiche la somme de toute les ligne
    grâce au click sur le bouton 
*/
$(document).ready(function () {

    $("#btnAdd").click(Ajouter);        //Quand on clique sur le btnAdd appel la fonction ajouter
    $("#btnDel").click(Supprimer);      //Quand on clique sur le btnDel appel la fonction Supprimer
      
        function Ajouter() {            //Fonction qui ajoute une ligne avec le numéros de sa ligne
            let res = 0;                //Variable pour numéros de ligne
            res = $("#tab tr").length + 1;  //Calcule le numéros de la ligne

            $("#tab").append("<tr><td>" + res + "</td></tr>") ; //Ajoute une ligne a la fin de tab
            $("#fin").html("<tr><td>" + calculTotal() + "</td></tr>");  //Change le tfoot et appel calculTotal pour faire la somme
        };

        function Supprimer() {      //Fonction qui supprime une ligne 
            $("#tab tr:last").remove(); //Suppréssion de la dernière ligne
            $("#fin").html("<tr><td>" + calculTotal() + "</td></tr>");  //Change tfoot et appel calculTotal pour refaire la somme 
        };
        
        function calculTotal() {    //Fonction qui retourne la somme des valeurs dans les lignes
            
            let res2 = 0;

            $("#tab td").each(function (index) {    //Boucle each qui parcour tout les td qui se trouve dans #tab
                let num = parseInt($(this).html()); //Affecte dans la variable num le numéros de la ligne
                res2 += num;                        //Incrémente à chaque passage
            });

        return res2;                                //Renvoie le résultat
        };
});
