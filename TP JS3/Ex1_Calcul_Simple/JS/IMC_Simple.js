/*
    Desmarets Simon
    Affiche l'IMC en fonction de la taille et du poid
*/
$(document).ready(function (){

    //Appel la fonction affIMC quand on click sur btnCalculIMC
    $("#btnCalculImc").click(affIMC);


function affIMC(){
    let poids = $("#idPoids").val();    //Variable qui lis le poid saisi
    let taille = $("#idTaille").val();  //Variable qui lis la taille saisie
    let imc = 0;

    //remplace les , par des .
    poids = poids.replace(",", ".");    
    taille = taille.replace(",", "."); 
    
    //Force en nombre
    poids = Number(poids);
    taille = Number(taille);

    //DEBUT SI
    if (isNaN(poids) || isNaN(taille)){ //Si le poid ou la taille ne sont pas des nombres
        $("#textIMC").html("ERREUR !!!!");  //Afficher erreur
    }else{                                  //SINON
        imc = calculerIMC(poids, taille);   //Appel de la fonction calculer IMC
        $("#textIMC").html(imc.toFixed(1)); //Afficher l'imc dans textIMC
    }  
    //FIN SI    
}   

function calculerIMC(prmPoids, prmTaille){
    //Retourne la valeur de l'IMC
    //2 prm : le poid et la taille
    let valRetour = prmPoids / ((prmTaille * prmTaille)/10000); //Calcul l'imc
    return valRetour;
}

});
