/*
    Desmarets Simon
    Affiche L'IMC et interprète le résultat
*/
$(document).ready(function (){

    //Appel la fonction affIMC quand on click sur btnCalculIMC
    $("#btnCalculImc").click(affIMC);


    function affIMC(){
        let poids = $("#idPoids").val();    //Variable qui lis le poid saisi
        let taille = $("#idTaille").val();  //Variable qui lis la taille saisie
        let imc = 0;
    
        //remplace les , par des .
        poids = poids.replace(",", ".");    
        taille = taille.replace(",", "."); 
        
        //Force en nombre
        poids = Number(poids);
        taille = Number(taille);
    
        //DEBUT SI
        if (isNaN(poids) || isNaN(taille)){ //Si le poid ou la taille ne sont pas des nombres
            $("#textIMC").html("ERREUR !!!!");  //Afficher erreur
        }else{ 
        imc = calculerIMC(poids, taille);   //Appel de la fonction calculer IMC
        inter = interpreterIMC(imc);        //Appel interpreterIMC
        $("#textIMC").html(imc.toFixed(1) + inter); //Affichage IMC + interprétation
    }      
}   

function calculerIMC(prmPoids, prmTaille){
    //Retourne la valeur de l'IMC
    //2 prm : le poid et la taille
    let valRetour = prmPoids / ((prmTaille * prmTaille)/10000); //Calcul l'imc
    return valRetour;
}

function interpreterIMC(prmValImc) { 
    //Retourne l'interprétation de l'IMC
    //1 prm : La valeur de l'imc
    let interpretation = "";
    
    if (prmValImc<16.5) {   //Si IMC inférieur a 16.5
        interpretation = " dénutrition";
    }else if (prmValImc >= 16.5 && prmValImc < 18.5) {  //Si IMC entre 16.5 et 18.5
        interpretation = " maigreur";
    }else if (prmValImc >= 18.5 && prmValImc < 25) {    //Si IMC entre 18.5 et 25
        interpretation = " corpulence normale"
    }else if (prmValImc >= 25 && prmValImc < 30) {  //Si IMC entre 25 et 30
        interpretation = " surpoids";
    }else if (prmValImc >= 30 && prmValImc < 35) {  //Si IMC entre 30 et 35
        interpretation = " obésité modérée";
    }else if (prmValImc >= 35 && prmValImc < 40) {  //Si IMC entre 35 et 40
        interpretation = " obésité sévère";
    }else if(prmValImc >= 40){                      //Si IMC supérieur à 40
        interpretation = " obésité morbide";
    }

    return interpretation; 
}

});
