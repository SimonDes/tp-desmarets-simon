/*
    Desmarets Simon
    Affiche L'IMC et interprète le résultat avec curseur et niveau couleur
*/
$(document).ready(function (){


    $("#idSliderPoids").on('input', function () {  
        affIMC(); 
    });

    $("#idSliderTaille").on('input', function () {  
        affIMC(); 
    });


    function affIMC(){
        let poids = $("#idSliderPoids").val();      //Variable qui lis le poid saisi
        let taille = $("#idSliderTaille").val();    //Variable qui lis la taille saisie
    
        $("#textPoids").html(poids);                //Affiche poid
        $("#textTaille").html(taille);              //Affiche taille
    
        imc = calculerIMC(poids, taille);           //Calcul IMC
        inter = interpreterIMC(imc);                //Interprete IMC
        $("#textIMC").html(imc.toFixed(1) + inter); //Affiche IMC et interprétation
        $("#balance").html(afficherBalance(imc));   //Appel fonction pour balance
         
    }   

function calculerIMC(prmPoids, prmTaille){
    //Retourne la valeur de l'IMC
    //2 prm : le poid et la taille
    let valRetour = prmPoids / ((prmTaille * prmTaille)/10000); //Calcul l'imc
    return valRetour;
}

function interpreterIMC(prmValImc) { 
    //Retourne l'interprétation de l'IMC
    //1 prm : La valeur de l'imc
    let interpretation = "";
    
    if (prmValImc<16.5) {   //Si IMC inférieur a 16.5
        interpretation = " dénutrition";
    }else if (prmValImc >= 16.5 && prmValImc < 18.5) {  //Si IMC entre 16.5 et 18.5
        interpretation = " maigreur";
    }else if (prmValImc >= 18.5 && prmValImc < 25) {    //Si IMC entre 18.5 et 25
        interpretation = " corpulence normale"
    }else if (prmValImc >= 25 && prmValImc < 30) {  //Si IMC entre 25 et 30
        interpretation = " surpoids";
    }else if (prmValImc >= 30 && prmValImc < 35) {  //Si IMC entre 30 et 35
        interpretation = " obésité modérée";
    }else if (prmValImc >= 35 && prmValImc < 40) {  //Si IMC entre 35 et 40
        interpretation = " obésité sévère";
    }else if(prmValImc >= 40){                      //Si IMC supérieur à 40
        interpretation = " obésité morbide";
    }

    return interpretation; 
}

function afficherBalance(prmValImc) {
    //Affiche la balance avec 1 prm
    let deplacement = 0;                 //Variable de déplacement 
    if (prmValImc >= 10 && prmValImc <= 45) {
        deplacement = (prmValImc - 10) * 8.571;
    }

    //Décale l'aiguille en fonction de la valeur de l'IMC
    $("#aiguille").css("left", deplacement + "px");
}

});
